#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WINDOWS
#include <windows.h>
#else
    #include <unistd.h>
    #define Sleep(x) usleep((x)*1000)
#endif
#define SIZ 9

int sudoku[SIZ][SIZ];
int viewmode = 0;
int x;
int y;

int looking_through(int sudoku[]) {
    int changed;
    int nofinish;
    int smallestdecision;
    int sd_x, sd_y;
    int decisions[SIZ];
    do {
        smallestdecision = SIZ+1;
        changed = 0;
        nofinish = 0;
        for (y = 0; y < SIZ; y++) {
            for (x = 0; x < SIZ; x++) {
                if (sudoku[y*SIZ + x] == 0) {
                    nofinish = 1;
                    int row[SIZ] = {0};
                    int rc;
                    for (rc = 0; rc < SIZ; rc++) {
                        row[sudoku[y*SIZ + rc]-1] = 1;
                        row[sudoku[rc*SIZ + x]-1] = 1;
                    }
                    int rx;
                    for (rc = (y/3)*3; rc < (y/3)*3+3; rc++) {
                        for (rx = (x/3)*3; rx < (x/3)*3+3; rx++ ) {
                            row[sudoku[rc*SIZ + rx]-1] = 1;
                        }
                    }
                    rx = 0;
                    for (rc = 0; rc < SIZ; rc++) {
                        if (!row[rc]) {
                            sudoku[y*SIZ + x] = rc+1;
                            rx++;
                        }
                    }
                    if (rx == 0) {
                        return 0;
                    }
                    if (rx == 1) {
                        changed = 1;
                    } else {
                        if (smallestdecision > rx) {
                            smallestdecision = rx;
                            sd_x = x;
                            sd_y = y;
                            int fillid = 0;
                            for (rc = 0; rc < SIZ; rc++) {
                                if (!row[rc]) {
                                    decisions[fillid] = rc+1;
                                    fillid++;
                                }
                            }
                        }
                        sudoku[y*SIZ + x] = 0;
                    }
                }
            }
        }
    } while (changed != 0);
    if (nofinish == 0) {
        if ((viewmode == 2) || (viewmode == 3)) {printf("Difficulty: ");}
        return 1;
    }
    for (changed = 0; changed < smallestdecision; changed++ ) {
        int *arrcopy = (int*) malloc(sizeof(int) * SIZ * SIZ);
        for (int fillid = 0; fillid<SIZ * SIZ;fillid++) {
            arrcopy[fillid] = sudoku[fillid];
        }
        arrcopy[sd_y*SIZ + sd_x] = decisions[changed];
        if (looking_through(arrcopy)) {
            if ((viewmode == 2) || (viewmode == 3)) {printf("|");}
            for (int fillid = 0; fillid<SIZ * SIZ;fillid++) {
                 sudoku[fillid] = arrcopy[fillid];
            }
            return 1;
        }
        free(arrcopy);
    }
    return 0;
}
int main(int argc, char *argv[]) {
    FILE *fp;
    int i = 1;
    if (argc > 1) {
        viewmode = -1;
        if (!strcmp(argv[1], "-i")) {
            viewmode = 1;i=2;
        }
        if (!strcmp(argv[1], "-d")) {
            viewmode = 2;i=2;
        }
        if ((!strcmp(argv[1], "-id")) || (!strcmp(argv[1], "-di"))) {
            viewmode = 3;i=2;
        }
        if (argv[1][0] != '-') {
            viewmode = 100;
        }
    } 
    if (viewmode <= 0) {
        printf("Usage: %s [OPTION] [FILE]...\nSudoku solver https://gitlab.com/Daniel_Toth/sudoku-solver\n\n  -i \tshow input\n  -d \tshow difficulty\n  -id\tshow input and difficulty\n", argv[0]);
        Sleep(1000);
        return 0;
    }
    for (; i<argc; i++) {
        x = y = 0;
        char* filename = argv[i];
        fp = fopen(filename, "r");
        if (fp == NULL){
            printf("Error: Could not open file %s",filename);
            return 1;
        }
        while (fscanf(fp,"%d", &sudoku[y][x]) == 1) {
            if (x<SIZ-1) {
                x++;
            } else {
                x = 0;
                y++;
            }
            if (x==1 && y==SIZ) {
                printf("Error: incorrect input");
                exit(0);
            }
        }
        if ((viewmode == 1) || (viewmode == 3)) {
            for (y = 0; y < SIZ; y++) {
                for (x = 0; x < SIZ; x++) {
                    printf("%d ", sudoku[y][x]);
                }
                printf("\n");
            }
        }
        if (viewmode == 1) { printf("\n"); }

        looking_through(&sudoku[0][0]);

        if ((viewmode == 2) || (viewmode == 3)) {printf("\n");}
        for (y = 0; y < SIZ; y++) {
            for (x = 0; x < SIZ; x++) {
                printf("%d ", sudoku[y][x]);
            }
            printf("\n");
        }
        if ((i+1)<argc) {
            printf("\n");
        }
    }
}
